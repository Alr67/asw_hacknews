class User < ActiveRecord::Base
  include ActionView::Helpers::DateHelper
  has_many :contributions
  has_many :comments
  acts_as_voter
  before_create :set_auth_token
    
  def self.find_or_create_from_auth_hash(auth_hash)
      #Busquem si l'usuari existeix o si l'hem de crear
      user = where(provider: auth_hash.provider, uid: auth_hash.uid).first_or_create
      user.update(
        username: auth_hash.info.name,
        profile_image: auth_hash.info.image,
        token: auth_hash.credentials.token,
        secret: auth_hash.credentials.secret
        )
      return user
  end
  
  def get_voted_contributions
    return self.find_up_voted_items.size
  end 
  
  def hoursAgo
    distance_of_time_in_words_to_now(created_at)
  end
  
  def userURL
      "https://enigmatic-citadel-88968.herokuapp.com/users/" + id.to_s
  end
  
  private
  def set_auth_token
    return if auth_token.present?
    self.auth_token = generate_auth_token
  end

  def generate_auth_token
    SecureRandom.uuid.gsub(/\-/,'')
  end
  
end
