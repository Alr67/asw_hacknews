class Contribution < ActiveRecord::Base
  include ActionView::Helpers::DateHelper
  belongs_to :user
  has_many :comments
  before_save :default_values
  acts_as_votable
  def default_values
    self.user = User.find(user_id)
    self.puntuacio = 0
    self.created_at = DateTime.current
  end
  
  def getUserName
    if user
      user.username
    else return "no te user"
    end
  end
  
  def getUserId
    if user
      user_id
    else return "no te user"
    end
  end
  
  def numComments
    comments.count
  end
  
  def self.new_contribution(url,text)
    case type
    when 'url'
      Url_contribution.new
    when 'text'
      Ask_contribution.new
    end
  end
  
  def isAsk
    if text == "" || text == nil
      return false
    else return true
    end
  end
  
  def isURL
    if url == "" || url == nil
      return false
    else return true
    end
  end
  
  def findUrls
    Contribution.all 
  end
  
  def getToken
    user.token
  end
  
  def numVotes
    self.get_upvotes.size
  end
  
  def numComments
    comments.size
  end
  
  def userName
    if user
      user.username
    else return "no te user"
    end
  end
  
  def userURL
    if user
      "https://enigmatic-citadel-88968.herokuapp.com/users/" + user_id.to_s
    else return "no te user"
    end
  end
  
  def hoursAgo
    distance_of_time_in_words_to_now(created_at)
  end
  
  def showURL
    "https://enigmatic-citadel-88968.herokuapp.com/contributions/" + id.to_s
  end
  
  
  def getComments
    @filtred = Array.new
    comments.reverse_each do |variable|
      @filtred.push(variable.as_json(methods: [:userName,:hoursAgo,:numVotes,:getContribution,:getReplies],
                                          except: [:created_at]))
    end
    return @filtred
  end
end
