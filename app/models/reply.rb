class Reply < ActiveRecord::Base
    include ActionView::Helpers::DateHelper
    belongs_to :comment
    belongs_to :user
    acts_as_votable

    def default_values
        self.user_id ||= 1
        self.user ||= User.find(self.user_id)
        self.created_at ||= DateTime.current
    end
    
    def getUserName
        self.user.username
    end
    
    def getContribution
        self.comment.contribution
    end
    
    def hoursAgo
        distance_of_time_in_words_to_now(created_at)
    end
    
    def userName
        if user
            user.username
        else return "no te user"
        end
    end
  
    def userURL
        if user
            "https://enigmatic-citadel-88968.herokuapp.com/users/" + user_id.to_s
        else return "no te user"
        end
    end
    
    def numVotes
        self.get_upvotes.size
    end
  
end
