class Comment < ActiveRecord::Base
    include ActionView::Helpers::DateHelper
    belongs_to :user
    belongs_to :contribution
    has_many :replies
    acts_as_votable
    
    def getUserName
        if user 
          self.user.username
        else
          return "noUser"
        end
    end
    
    def getUserId
      if user
        self.user.id
      else
        return 1
      end
    end
    
    def getContribution
      return self.contribution
    end
    
    def hoursAgo
      distance_of_time_in_words_to_now(created_at)
    end
    
    def userName
      if user
          user.username
      else return "no te user"
      end
    end
  
    def userURL
      if user
          "https://enigmatic-citadel-88968.herokuapp.com/users/" + user_id.to_s
      else return "no te user"
      end
    end
    
    def numVotes
      self.get_upvotes.size
    end
    
    def numReplies
      self.replies.size
    end
    
    def getReplies
      @filtredRep = Array.new
    replies.reverse_each do |variable|
      @filtredRep.push(variable.as_json(methods: [:userName,:hoursAgo,:numVotes],
                                          except: [ :created_at]))
    end
    return @filtredRep
    end
      
end
