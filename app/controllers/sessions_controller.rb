class SessionsController < ApplicationController
  def create
    @user = User.find_or_create_from_auth_hash(auth_hash)
    session[:user_id] = @user.id
    session[:username] = @user.username
    
    if(session[:comment])
      redirect_to Contribution.find(session[:contribution_id])
      return
    end
    if(session[:reply])
      redirect_to new_reply_path
      return
    end
    if(session[:body])
      redirect_to new_contribution_path
    else
      redirect_to root_path
    end
  end
  
  def delete_session
    @user = nil
    session.delete(:user_id)
    session.delete(:username)
    redirect_to root_path
  end
  
  helper_method :delete_session

  protected

  def auth_hash
    request.env['omniauth.auth']
  end
end