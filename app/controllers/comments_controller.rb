class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]

  # GET /comments
  # GET /comments.json
  # DONEEEEEEEEEEEEEEEE
  def index
    @comments = Comment.all
    @filtred = Array.new
    @comments.reverse_each do |variable|
      @filtred.push(variable.as_json(methods: [:userName,:hoursAgo,:numVotes,:getContribution],
                                          except: [:created_at]))
    end
    respond_to do |format|
      format.html {}
      format.json {
        authenticate
        render :json => @filtred
      }
    end
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
  end
  
  def upvote 
    if current_user 
      @comment= Comment.find(params[:id])
      @comment.upvote_by current_user
    else  
      information = request.raw_post
      data_parsed = JSON.parse information
      @comment = Comment.find(data_parsed["id"])
      @comment.upvote_by authenticate
    end
    
    respond_to do |format|
        format.js {render inline: "location.reload();" }
        format.json {
          authenticate
          render :json => @comment.as_json()
        }
    end 
  end  

  def downvote
    @comment = Comment.find(params[:id])
    @comment.downvote_by current_user
    redirect_to :back
  end

  # GET /comments/new
  def new
    @comment = Comment.new
  end

  # GET /comments/1/edit
  def edit
  end

  # POST /comments
  # POST /comments.json
  def create
    
    respond_to do |format|
      format.html {
        if !session[:user_id]
          session[:contribution_id] = comment_params[:contribution_id]
          session[:comment] = true
          session[:body] = comment_params[:body]
          redirect_to "/auth/twitter" and return
        end
      }
      format.json{}
    end
    
    respond_to do |format|
      format.html {}
      format.json {
        if !Contribution.exists?(comment_params[:contribution_id])
            raise ActionController::ParameterMissing.new("There is no contribution with this id")
        end
      }
    end
    
    if session[:user_id] || authenticateCreation
      @comment = Comment.new(comment_params)
      if(session[:user_id])
        @comment.user= User.find(session[:user_id])
      end
      if(authenticateCreation) 
        @comment.user = session[:user_creation]
        information = request.raw_post
        data_parsed = JSON.parse(information)
        @comment.contribution_id = data_parsed["contribution_id"]
      end 
      
      respond_to do |format|
        if @comment.save
          format.html { redirect_to @comment.contribution, notice: 'Comment was successfully created.' }
          format.json { 
            render :json => @comment.as_json()
          }
        else
          format.html { render :new }
          format.json { render json: @comment.errors, status: :unprocessable_entity }
        end
      end
    end
  end 
  

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to @comment, notice: 'Comment was successfully updated.' }
        format.json { render :show, status: :ok, location: @comment }
      else
        format.html { render :edit }
        format.json { render json: @comment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    respond_to do |format|
        format.html {}
        format.json { 
          if @comment.user != authenticate 
            raise ActionController::ParameterMissing.new("You cannot delete a comment that is not yours")
          end
        }
    end
      
      Reply.all.each do |reply|
        if reply.comment == @comment
          reply.destroy
        end
      end
    
      @comment.destroy
      respond_to do |format|
        format.html { redirect_to :back, notice: 'Comment was successfully destroyed.' }
        format.json { head :no_content }
      end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_comment
      @comment = Comment.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def comment_params
      respond_to do |format|
        format.html { }
        format.json { raise ActionController::ParameterMissing.new("Format error: empty contribution_id") if params[:contribution_id].blank?
      raise ActionController::ParameterMissing.new("Format error: empty body") if params[:body].blank?}
      end
      
      params.require(:comment).permit( :contribution_id, :body, :user_id)
    end
    
    protected
    def authenticate
      authenticate_or_request_with_http_token do |token, options|
        User.find_by(auth_token: token)
      end
    end
    
    protected
    def authenticateCreation
      if session[:user_id]
        return false
      else
        authenticate_or_request_with_http_token do |token, options|
          session[:user_creation] = User.find_by(auth_token: token)
        end
      end
    end
    
end

