class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index #AQUI ALEJANDROOOOOOOOOOOOO
    @users = User.all
    respond_to do |format|
      format.html {}
      format.json {
        authenticate
        render :json => @users.as_json(methods: [:hoursAgo, :userURL], except: [:token, :secret, :provider, :auth_token, :updated_at, :uid, :created_at])
      }
    end
  end

  # GET /users/1
  # GET /users/1.json
  def show #AQUI ALEJANDROOOOOOOOOOOOO
    respond_to do |format|
      format.html {}
      format.json {
        if @user == authenticate
          render :json => @user.as_json(methods: [:hoursAgo, :userURL], except: [:provider,:secret,:token,:updated_at,:created_at])
        else
          render :json => @user.as_json(methods: [:hoursAgo, :userURL], except: [:token,:provider,:secret,:auth_token,:updated_at,:created_at])
        end
      }
    end
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
  
    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def apicreate 
    @user = User.new()
    @user.username = params[:username]
    format.html { render :new }
    format.json { render json: @user.errors, status: :unprocessable_entity }
    
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
        format.html {}
        format.json { 
          if @user != authenticate 
            raise ActionController::ParameterMissing.new("You cannot update a user that is not you")
          end
        }
    end
    
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      if params[:id]
        @user = User.find(params[:id])
      else
        @user = User.find(params[:uid])
      end
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:id, :profile_image, :about, :mail)
    end
    
    def getUser 
      return current_user.username
    end
    
    protected
    def authenticate
      authenticate_or_request_with_http_token do |token, options|
        User.find_by(auth_token: token)
      end
    end
end
