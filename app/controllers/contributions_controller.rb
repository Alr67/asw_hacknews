class ContributionsController < ApplicationController
  before_action :set_contribution, only: [:show, :edit, :update, :destroy]


  helper_method :isLiked
  # GET /contributions
  # GET /contributions.json   
  # DONEEEEEEEEEEEEEEEE
  def index
    @contributions = Contribution.all
    @filtred = Array.new
    @contributions.reverse_each do |variable|
        if variable.isURL
          @filtred.push(variable.as_json(methods: [:numVotes, :userName, :userURL, :hoursAgo, :numComments, :showURL, :getComments], 
                                          except: [:text, :updated_at, :puntuacio, :created_at]) )
        end
    end
    respond_to do |format|
      format.html {}
     # format.json render :partial => "contributions/ask.json"
  
      format.json { #es como hacer un IF
        #authenticate
        render :json => @filtred
      }
    end
  end
  
  
  # GET /contributions/ask
  # GET /contributions/ask.json
  # DONEEEEEEEEEEEEEEEE
  def ask
    @contributions = Contribution.all
    @filtred = Array.new
    @contributions.reverse_each do |variable|
        if variable.isAsk
          @filtred.push(variable.as_json(methods: [:numVotes, :userName, :userURL, :hoursAgo, :numComments, :showURL,:getComments]) )
        end
    end
    respond_to do |format|
      format.html {}
     # format.json render :partial => "contributions/ask.json"
      
      format.json { #es como hacer un IF
        authenticate
        render :json => @filtred
      }
    end
  end
  
  # GET /thread/1
  # GET /thread.json
  # DONEEEEEEEEEEEEEEEE
  def thread
    @contributions = Contribution.all
    @filtred = Array.new
    @contributions.reverse_each do |variable|
        if variable.user == User.find(params[:id])
          if variable.isAsk
            @filtred.push(variable.as_json(methods: [:numVotes, :userName, :userURL, :hoursAgo, :numComments, :showURL], 
                                          except: [:url, :updated_at, :puntuacio, :created_at]) )
          end
          if variable.isURL
            @filtred.push(variable.as_json(methods: [:numVotes, :userName, :userURL, :hoursAgo, :numComments, :showURL], 
                                          except: [:text, :updated_at, :puntuacio, :created_at]) )
          end
        end
    end
    respond_to do |format|
      format.html {}
      format.json { #es como hacer un IF
        authenticate
        render :json => @filtred
      }
    end
  end

# GET /contributions/new
  def new
    @contribution = Contribution.new
  end

  # GET /contributions/1
  # GET /contributions/1.json
  # DONEEEEEEEEEEEEEEEE
  def show
    respond_to do |format|
      format.html {}
      format.json {
        authenticate
        #Escrivim la contribucio en funcio de si es ASK o URL
        if @contribution.isAsk                  #De la Contribution
          render :json => @contribution.as_json(methods: [:numVotes, :userName, :userURL, :hoursAgo, :numComments, :showURL], 
                                                except: [:url, :updated_at, :puntuacio, :user_id, :created_at],  
                                                          #De cada comment de la contribution
                                                include: { comments: {  #De cada reply del comentari
                                                          include: { replies: { 
                                                                    except: [:updated_at, :comment_id, :created_at],
                                                                    methods: [:numVotes, :userName, :userURL, :hoursAgo]} }, 
                                                          except: [:updated_at, :contribution_id, :created_at],
                                                          methods: [:numVotes, :userName, :userURL, :hoursAgo]} })
        end
        if @contribution.isURL                  #De la Contribution
          render :json => @contribution.as_json(methods: [:numVotes, :userName, :userURL, :hoursAgo, :numComments, :showURL], 
                                                except: [:text, :updated_at, :puntuacio, :user_id, :created_at, :id], 
                                                          #De cada comment de la contribution
                                                include: { comments: {  #De cada reply del comentari
                                                          include: { replies: { 
                                                                    except: [:updated_at, :comment_id, :user_id, :created_at],
                                                                    methods: [:numVotes, :userName, :userURL, :hoursAgo]} }, 
                                                          except: [:updated_at, :contribution_id, :created_at, :user_id],
                                                          methods: [:numVotes, :userName, :userURL, :hoursAgo]} })
        end
      }
    end
  end

 # PUT 
  def upvote 
    if current_user 
      @contribution= Contribution.find(params[:id])
      @contribution.upvote_by current_user
    end
    if !current_user 
      information = request.raw_post
      data_parsed = JSON.parse information
      @contribution = Contribution.find(data_parsed["id"])
      @contribution.upvote_by authenticate
    end
    respond_to do |format|
        format.js {render inline: "location.reload();" }
        format.json {
          authenticate
          #Escrivim la contribucio en funcio de si es ASK o URL
          if @contribution.isAsk                  #De la Contribution
            render :json => @contribution.as_json(methods: [:numVotes, :userURL, :userName, :showURL], 
                                                  except: [:url, :updated_at, :puntuacio, :user_id, :created_at])
          end
          if @contribution.isURL                  #De la Contribution
            render :json => @contribution.as_json(methods: [:numVotes, :userURL, :userName, :showURL], 
                                                  except: [:text, :updated_at, :puntuacio, :user_id, :created_at])
          end
      }
    end
  end  

  def downvote
    @contribution = Contribution.find(params[:id])
    @contribution.downvote_by current_user
    redirect_to :back
  end

  # GET /contributions/1/edit
  def edit
  end

  # POST /contributions
  # POST /contributions.json
  def create #-----> Hay q enviar siempre TITLE y los 2 parametros, pero dejando 1 vacio
    
    respond_to do |format|
      format.html {
        if !session[:user_id]
          session[:body] = contribution_params[:title]
          session[:url] = contribution_params[:url]
          session[:text] = contribution_params[:text]
          redirect_to "/auth/twitter" and return
        end
      }
      format.json{}
    end
    
    if (contribution_params[:url]!=nil && contribution_params[:url]!="" && contribution_params[:text] != nil && contribution_params[:text] !="")
      session[:body] = contribution_params[:title]
      session[:url] = contribution_params[:url]
      session[:text] = contribution_params[:text]
      respond_to do |format|
         format.html { redirect_to new_contribution_path, notice: "fill just url or text"}
         format.json { 
            raise ActionController::ParameterMissing.new("You cannot create a contribution with text and URL") 
         }
      end
    
    elsif (contribution_params[:text] == nil || contribution_params[:text]=="") && (contribution_params[:url] == nil || contribution_params[:url]=="")
      session[:body] = contribution_params[:title]
      respond_to do |format|
         format.html { redirect_to new_contribution_path, notice: "Fill text or url"}
         format.json {}
      end
      
    elsif contribution_params[:title] == nil || contribution_params[:title] == ""
      session[:url] = contribution_params[:url]
      session[:text] = contribution_params[:text]
      respond_to do |format|
         format.html { redirect_to new_contribution_path, notice: "Ha de tenir un titol"}
         format.json { raise ActionController::ParameterMissing.new("You cannot create a contribution without title") }
      end

    else 
      @contribution = Contribution.new(contribution_params)
      if(authenticateCreation) 
          @contribution.user = authenticateCreation  
      else
        @contribution.user = User.find(session[:user_id]) 
      end
      respond_to do |format|
        if @contribution.save
          format.html { redirect_to contributions_path, notice: 'Contribution was successfully created.' }
          format.json { render :show, status: :created, location: @contribution }
        else
          format.html { render :new }
          format.json { render json: @contribution.errors, status: :unprocessable_entity }
        end
      end
    end
  end
  

  # PATCH/PUT /contributions/1
  # PATCH/PUT /contributions/1.json
  def update
    
    if @contribution.isAsk  
      if contribution_params[:url]
        respond_to do |format|
          format.html { redirect_to @contribution, notice: "This is an ask, can't be a URL" and return} 
          format.json { raise ActionController::ParameterMissing.new("You can't fill URL in a text contribution") }
        end
      end
    
    elsif @contribution.isURL  
      if contribution_params[:text] 
        respond_to do |format|
          format.html { redirect_to @contribution, notice: "This is an URL, can't be a text" and return} 
          format.json { 
            if contribution_params[:text] != ""
              raise ActionController::ParameterMissing.new("You can't fill text in a URL contribution") 
            end
          }
        end
      end
    end
      
    
    respond_to do |format|
        format.html {}
        format.json { 
          if @contribution.user != authenticate 
            raise ActionController::ParameterMissing.new("You cannot update a contribution that is not yours")
          end
        }
    end
    respond_to do |format|
      if @contribution.update(contribution_params)
        format.html { redirect_to @contribution, notice: 'Contribution was successfully updated.' and return } 
        format.json { render :json => @contribution.as_json() }
      else
        format.html { render :edit }
        format.json { render json: @contribution.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contributions/1
  # DELETE /contributions/1.json
  def destroy
    respond_to do |format|
        format.html {}
        format.json { 
          if @contribution.user != authenticate 
            raise ActionController::ParameterMissing.new("You cannot destroy a contribution that is not yours")
          end
        }
    end
    
    Comment.all.each do |comment|
      if comment.contribution == @contribution
        Reply.all.each do |reply|
          if reply.comment == comment
            reply.destroy
          end
        end
        comment.destroy
      end 
    end
    @contribution.destroy
    respond_to do |format|
      format.html { redirect_to contributions_url, notice: 'Contribution was successfully destroyed.' }
      format.json { head :no_content }
    end
  end
  

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_contribution
      @contribution = Contribution.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def contribution_params
      params.permit(:title, :url, :text, :user_id, :id)
    end
    
    protected
    def authenticate
      authenticate_or_request_with_http_token do |token, options|
        return User.find_by(auth_token: token)
      end
    end
    
    def authenticateCreation
      if session[:user_id]
        return false
      else
        authenticate_or_request_with_http_token do |token, options|
          session[:user_creation] = User.find_by(auth_token: token) 
        end
      end
    end
  
end
