class RepliesController < ApplicationController
  before_action :set_reply, only: [:show, :edit, :update, :destroy]

  # GET /replies
  # GET /replies.json
  def index #AQUI ALEJANDROOOOOOOOOOOOO
    @replies = Reply.all
    respond_to do |format|
      format.html {}
      format.json {
        authenticate
        render :json => @replies.as_json
      }
    end
  end

  # GET /replies/1
  # GET /replies/1.json
  def show
  end

  # GET /replies/new
  def new
    @reply = Reply.new
  end

  # GET /replies/1/edit
  def edit
  end
  
  def upvote
    
    if current_user 
      @reply= Reply.find(params[:id])
      @reply.upvote_by current_user
    end
    if !current_user 
      information = request.raw_post
      data_parsed = JSON.parse information
      @reply = Reply.find(data_parsed["id"])
      @reply.upvote_by authenticate
    end
    
    respond_to do |format|
        format.js {render inline: "location.reload();" }
        format.json {
          authenticate
          render :json => @reply.as_json()
      }
    end
  end  

  def downvote
    @reply = Reply.find(params[:id])
    @reply.downvote_by current_user
    redirect_to :back
  end

  # POST /replies
  # POST /replies.json
  def create
    
    respond_to do |format|
      format.html {
        if !session[:user_id]
          session[:comment_id] = reply_params[:comment_id]
          session[:reply] = true
          session[:body] = reply_params[:body]
          redirect_to "/auth/twitter" and return
        end
      }
      format.json{}
    end
    
    respond_to do |format|
      format.html {}
      format.json {
        if !Comment.exists?(reply_params[:comment_id])
            raise ActionController::ParameterMissing.new("You cannot create a replay to a non existing comment")
        end
      }
    end
    if session[:user_id] || authenticateCreation
      @reply = Reply.new(reply_params)
      if(session[:user_id])
        @reply.user= User.find(session[:user_id])
      end
      
      if(authenticateCreation) 
        @reply.user = authenticateCreation
        information = request.raw_post
        data_parsed = JSON.parse information
        @reply.comment_id = data_parsed["comment_id"]
        @reply.user_id = authenticate.id
      end 
      
      respond_to do |format|
        if @reply.save
          format.html { redirect_to @reply.getContribution, notice: 'Reply was successfully created.' }
          format.json { render :json => @reply.as_json() }
        else
          format.html { render :new }
          format.json { render json: @reply.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PATCH/PUT /replies/1
  # PATCH/PUT /replies/1.json
  def update
    respond_to do |format|
      if @reply.update(reply_params)
        format.html { redirect_to @reply, notice: 'Reply was successfully updated.' }
        format.json { render :show, status: :ok, location: @reply }
      else
        format.html { render :edit }
        format.json { render json: @reply.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /replies/1
  # DELETE /replies/1.json
  def destroy
    respond_to do |format|
        format.html {}
        format.json { 
          if @reply.user != authenticate 
            raise ActionController::ParameterMissing.new("You cannot delete a reply that is not yours")
          end
        }
    end
    @reply.destroy
    respond_to do |format|
      format.html { redirect_to :back, notice: 'Reply was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reply
      @reply = Reply.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reply_params
      params.require(:reply).permit(:comment_id, :body )
    end
    
    protected
    def authenticate
      authenticate_or_request_with_http_token do |token, options|
         User.find_by(auth_token: token)
      end
    end
    
    def authenticateCreation
      if session[:user_id]
        return false
      else
        authenticate_or_request_with_http_token do |token, options|
          session[:user_creation] = User.find_by(auth_token: token)
        end
      end
    end
end