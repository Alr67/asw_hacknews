json.array!(@comments) do |comment|
  json.extract! comment, :id, :answering_id, :contribution_id, :body, :created_at, :user_id
  json.url comment_url(comment, format: :json)
end
