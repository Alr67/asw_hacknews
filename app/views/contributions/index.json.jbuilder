json.array!(@contributions) do |contribution|
  json.extract! contribution, :id, :title, :puntuacio, :url, :text, :user_id
  json.url contribution_url(contribution, format: :json)
end
