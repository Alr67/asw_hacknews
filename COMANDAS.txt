ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDDce5wk5LZ5yE7Fp3cJkl9JvbPi/byXS/d5rLtpmNZ+c+NOSiOKATHlLzNkevX7y3T9cNAyzSdUb8tX0bxboHUCU3FDEhT4D1HkocYiYInzNQcUJN3jK4e4m7ZcbXP4+9PMaHu91dfr9+eOSZILb1R6fELQyxOXYSlanMMgJvryBpXkx1GWcqY0bdcF9+NQg+VZ1FNUznp4yL6nuS1pXUxwrnawQnrjZJZgLWkdHJxHIrt6goja00d6CuwUM4B4g/wQXIMWoBfK+qNSYpSMHyHaJ6AnBTiMVkVqB5n4HLCznEOL8fa99YQaX1UaGjHd+1JUGpaEW8ozBzB1fKZ2yqk3BjzIbAwcZ1VKlYXy6ttjSne27Tin1f4oHMAy1oXv21j98pTSs+aPtNksswfC4g+8wL/G1c93RmnLpLz3oZ5kwN8VzHZIrsJZj7BXXm4th1K5Gzp2DQlNZoBaXGRMMb98jYMLtpBTUQxgxWCIsVrWArHNBJ9Q15PHBzdaB7htT0sVW5mliMj7Z0dKOIVuhI7ToLPvHmR1cMTqBxpag+rav0hF9FFpb1aDEZ+MQuyIqkOa0ZO9jItH9CahuXWA4kLOxBsOC6Lk0NJ4u6DeJPoXY5+zrX8hjRksACC03NMWjTvSqwQA7ObOiswZ7ZDG25vGSv3lid7qSfKnq849dhmpw== rafael.lucena@est.fib.upc.edu


Server Heroku -->  enigmatic-citadel-88968.herokuapp.com

Repo BitBucket --> https://bitbucket.org/Alr67/asw_hacknews

Ejecutar server ----->  rails server -b $IP -p $PORT

Server de cloud9 ---->  https://asw-software33-alr67.c9users.io/

Push a heroku ---->     git push heroku master

Push de DB a Heroku --> heroku run rake db:migrate

Reset DB de Heroku --> heroku run rake db:reset
Instalar gemas ---->    bundle install --without production

forzar commit a ser head -->  git push --force origin ef01dfe:master

eliminar commits locales --> git reset --hard origin/master

Crear scaffold ---->    rails generate scaffold User name:string email:string
                        bundle exec rake db:migrate
                       
                        
Borrar toda la BD por si pasa algo--->  bundle exec rake db:drop:all 2> porqpeta.txt   
                                        bundle exec rake db:create:all 2> porqpeta.txt    
                                        bundle exec rake db:migrate 2> porqpeta.txt
                                        
Eliminar branch local -----> git branch -d the_local_branch

Crear branch -----> git checkout -b develop

Push de branca ----> git push origin develop

Tutorial login google -----> https://richonrails.com/articles/google-authentication-in-ruby-on-rails

Tutorial login facebook -----> https://richonrails.com/articles/facebook-authentication-in-ruby-on-rails

Tutorial gem upvote -----> http://www.mattmorgante.com/technology/votable


Tutorial herencia -----> https://dan.chak.org/enterprise-rails/chapter-10-multiple-table-inheritance/

Tutorial REST API -------> http://www.andrewhavens.com/posts/20/beginners-guide-to-creating-a-rest-api/

https://www.codeschool.com/blog/2014/02/03/token-based-authentication-rails/

