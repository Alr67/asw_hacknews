class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :provider
      t.string :uid
      t.string :username
      t.string :token
      t.string :secret
      t.string :profile_image
      t.string :about
      t.string :mail
      t.string :auth_token

      t.timestamps null: false
    end
  end
end
