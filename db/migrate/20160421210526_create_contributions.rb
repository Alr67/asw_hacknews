class CreateContributions < ActiveRecord::Migration
  def change
    create_table :contributions do |t|
      t.string :title
      t.integer :puntuacio
      t.string :url
      t.string :text
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
