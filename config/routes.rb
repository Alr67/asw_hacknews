Rails.application.routes.draw do
  resources :replies
  resources :users
  resources :contributions
  resources :authentications
  resources :replies
  resources :contributions
  resources :comments
  resources :users
  resources :asks
  resources :threads
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".
  
    get '/auth/:provider/callback', to: 'sessions#create'
    
  # You can have the root of your site routed with "root"
    root to: 'contributions#index'
    
    get 'delete_session', to: 'sessions#delete_session'
    
    get '/ask', to: 'contributions#ask'
    
    get '/thread/:id', to: 'contributions#thread'
    
    get "/users" => "users#index"
    
    get "/user/:id" => "users#show"
    
    get "/comments" => "comments#index"
    
    get "/contributions" => "contributions#index"
    
    get "/contribution/:id" => "contributions#show"
    
    get "/comments" => "comments#index"
    
    post "/user" => "users#create"
    
    post "/contributions/publish" => "contributions#create"
    
    post "/contribution/comment" => "comments#create"
    
    post "/contribution/comment/reply" => "replies#create"
    
    put "/contribution" => "contributions#upvote"
    
    put "/user/edit" => "users#update"
    
    put "/comment/delete"  => "comments#destroy"
    
    put "/contribution/edit" => "contributions#update"
    
    put "/contribution/delete" => "contributions#destroy"
    
    put "/reply/delete" => "replies#destroy"
    
    put "/contribution/upvote" => "contributions#upvote"
    
    put "/comment/upvote" => "comments#upvote"
    
    put "/reply/upvote" => "replies#upvote"
    
    put "/comment" => "comments#upvote"
    
    put "/reply" => "replies#upvote"

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
